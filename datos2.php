<!-- Por:Luz, y Andrea -->
<?php
	#Obtiene el id del index
	$id = $_GET["id"];

	require 'conexion.php';
	#Consulta para obtener información del maiz seleccionado
	$consultaM = "SELECT * FROM maiz WHERE id_maiz=".$id;
	$consultaE = "SELECT r.estado FROM maiz m, region r, `maiz-region` a WHERE m.id_maiz=a.id_maiz AND r.id_region=a.id_region AND m.id_maiz=".$id;

	$resultadoM = $conexion->query($consultaM);
	$resultadoE = $conexion->query($consultaE);

    # Conteo de filas de la tabla.
	$filasM = $resultadoM->num_rows;
	$filasE = $resultadoE->num_rows;

    # Si hay fila, imprimir cada renglon.
    if($filasM > 0){
        echo "<form action='eliminar.php'><div class='contenedorInformacion'><table class='tablaContenido'>";
        while($registroM = $resultadoM->fetch_assoc()){
			echo "<tr class='filas'><td class='columnas cTitulo' colspan='2'>" .$registroM["nombre"] ."</td></tr>";
			echo "<tr class='filas'><td class='columnas cImagen' colspan='2'><img src='img\\" .$registroM["imagen"] ."'</td></tr>";
			echo "<tr class='filas'><td class='columnas cDatos'>Longitud Hoja</td><td class='columnas fDatos'>" .$registroM["longitud_hoja"] ." cm</td></tr>";
			echo "<tr class='filas'><td class='columnas cDatos'>Ancho Hoja</td><td class='columnas fDatos'>" .$registroM["anchura_hoja"] ." cm</td></tr>";
			echo "<tr class='filas'><td class='columnas cDatos'>Longitud Espiga</td><td class='columnas fDatos'>" .$registroM["longitud_espiga"] ." cm</td></tr>";
			echo "<tr class='filas'><td class='columnas cDatos'>Longitud Espigilla</td><td class='columnas fDatos'>" .$registroM["longitud_espiguilla"] ." cm</td></tr>";
			echo "<tr class='filas'><td class='columnas cDatos'>Ancho Espigilla</td><td class='columnas fDatos'>" .$registroM["ancho_espiguilla"] ." cm</td></tr>";
			echo "<tr class='filas'><td class='columnas cDatos'>Nudos Mazorca</td><td class='columnas fDatos'>" .$registroM["nudos_mazorca"] ." cm</td></tr>";
			echo "<tr class='filas'><td class='columnas cDatos'>Mazorca Nudo</td><td class='columnas fDatos'>" .$registroM["mazorcas_nudo"] ." cm</td></tr>";
			if($filasE > 0){
				echo "<tr class='filas'><td class='columnas cTitulo' colspan='2'>Estados</td></tr>";
				while($registroE = $resultadoE->fetch_assoc()){
					echo "<tr class='filas'><td class='columnas cEstados' colspan='2'>" .$registroE["estado"] ."</td></tr>";
				}
			}
			echo "<tr class='filas'><td class='columnas cEstado' value='" .$registroM['id_maiz'] ."'><a class='enlaces' href='eliminar.php?id=".$registroM['id_maiz']."'>Eliminar</a></td><td class='columnas cEstado' value='" .$registroM['id_maiz'] ."'><a class='enlaces' href='actualizarMaiz.php?id=".$registroM["id_maiz"]."'>Actualizar</a></td></tr>";
		}

        echo "</table></div></form>";
	}
?>