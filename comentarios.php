<!-- Por: Alberto -->
<html>
    <head>
        <meta charset="utf-8">
        <title>Comentarios</title>
        <!-- Estilo barra de navegación -->
        <link rel="stylesheet" href="scripts/css/estilosBarraDeNavegacion.css">
        <!-- Estilo pie de página -->
        <link rel="stylesheet" href="scripts/css/estilosFooter.css">
        <!-- Estilo comentarios -->
        <link rel="stylesheet" href="scripts/css/estilosComentarios.css">
    </head>
    <body>
        <!-- Por: Alberto -->
        <div class="barraNavegacion">
            <ul class="menu">
                <li class="li"><a href="#" class="activo">Contacto</a></li>
                <li class="li"><a href="mapa.php">Mapa</a></li>
                <li class="li"><a href="index.php">Galer&iacute;a</a></li>
                <li class="li identidad"><img src="img/logo.png" class="logo"></li>
            </ul>
    	</div>

        <!-- Por: Alberto -->
        <div class="formulario">
            <div class="contenedorFormulario">
                <form autocomplete="off" action="enviarComentarios.php" name="comentarios" method="POST">
                    <label class="etiquetas">Nombre:</label>
                    <input class="entradaF" type="text" name="nombre" required>
                    <label class="etiquetas">Correo electr&oacute;nico:</label>
                    <input class="entradaF" type="email" name="correo" required>
                    <label class="etiquetas">Comentarios:</label>
                    <textarea name="comentario" class="areaTexto" required></textarea>
                    <div class="botonesFContenedor">
                        <input class="botonesF" type="submit" value="Enviar">
                        <input class="botonesF" type="reset" value="Restablecer">
                    </div>
                </form>
            </div>
        </div>

        <!-- Por: Arantxa -->
        <footer>
            <div id="footer">
                <div id="integrantes">
                    <ul class="integrantes">
                        <li>Cabrera Juárez Luz Del Carmen 2123064480</li>
                        <li>Domínguez Lara Andrea 2173070894</li>
                        <li>Garayzar Cristerna Arantxa 2163071727</li>
                        <li>Nieto Rocha Albertos 2163071736</li>
                    <ul>
                </div>
                <div id="contacto">
                    <p class="contacto"><strong>Información</strong></p>
                    <p>UEA: Programación de Web Dinámico <br> 
                    Trimestre: 19 Otoño <br>
                    Contacto: sotnotatam@maiceros.com</p>
                </div>
            </div>
        </footer>
    </body>
</html>