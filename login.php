<!-- Inicio de sesion administrador -->
<!-- Por: Alberto -->
<?php 
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Convocatoria - inicio administrado</title>
	<link rel="stylesheet" type="text/css" href="scripts/css/estiloLogin.css">
</head>
<body>
	<div class="contenedor">	
		<div class="inicio">
			<form action="iniciaAdmin.php" method="POST" autocomplete="off">
				<div class="contenedorImagen">
					<img src="img/logoLogin.png" alt="Imagen de inicio de usuario" class="imagen"><br/>
				</div>
				<div class="contenedorElementos">
					<label class="elementos">Nombre de usuario</label><br/>
					<input type="text" name="usr" required class="elementos"><br/>
					<label class="elementos">Contrase&ntilde;a</label><br/>
					<input type="password" name="psw" required class="elementos"><br/>
					<input type="submit" value="Iniciar Sesi&oacute;n" class="elementos botonIniciar"><br/>
				</div>
			</form>
		</div>
	</div>
</body>
</html>