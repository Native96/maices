<?php 
    // Registro de nuevo maíz.
    // Plantilla realizada por Alberto Nieto Rocha
    // Formulario y php escrito por Arantxa Garayzar
	// Modificado por Luz para mostra los datos en value.
    // Descripción: Muestra formulario al usuario Admin para agregar un nuevo registro de una raza de maíz
    //              ya que el usuario termine de llenar los datos, la sección php se encarga de procesar esos
	//              datos e ingresarlos a la BD maices. 
	
	// Sesión
	session_start();

	$id_maiz = $_GET["id"];

	require 'conexion.php';

	$consulta = "SELECT * FROM maiz WHERE id_maiz = ".$id_maiz;

	$resul = $conexion->query($consulta);

	$rows = $resul->num_rows;

?>
<!DOCTYPE html>
<html>
<head>
	<title>Modificar Maíz</title>
	<!-- Estilos Maiz -->
	<link rel="stylesheet" type="text/css" href="scripts/css/estiloActualizarMaiz.css">
</head>
<body>
	<div class="contenedor">	
		<div class="inicio">
			<form action="./validaMaiz.php" method="POST" id="maiz" >
				<div class="contenedorImagen">
                    <!-- Logotipo -->
					<img src="img/logoLogin.png" alt="Imagen de inicio de usuario" class="imagen"><br/>
				</div>
				<?php
					if($rows > 0){
						while($regis = $resul->fetch_assoc()){
							echo "<input type='hidden' name='id_maiz' value='".$id_maiz."'/>";
							echo "<div class='contenedorElementos'>";
							echo "<!-- Nombre del maiz -->";
							echo "<label class='elementos'>Nombre del maíz<input type='text' name='nombre' required class='elementos' value='".$regis["nombre"]."'><br/></label><br/>";
							echo "<!-- Seleccionar img -->";
                    		echo "<label class='elementos'>Seleccionar imagen<input type='file' name='imagen' id ='imagen' class='elementos' src='".$regis["imagen"]."'><br/></label><br/>";
							echo "<!-- Longitud de Hoja -->";
                    		echo "<label class='elementos'>Longitud de hoja<input type='number' step='0.01' name='longitud_hoja' required class='elementos' value='".$regis["longitud_hoja"]."'><br/></label><br/>";
							echo "<!-- Anchura Hoja -->";
							echo "<label class='elementos'>Anchura Hoja<input type='number' step='0.01' name='anchura_hoja' required class='elementos' value='".$regis["anchura_hoja"]."'><br/></label><br/>";
							echo "<!-- Longitud de Espiga -->";
							echo "<label class='elementos'>Longitud de Espiga<input type='number' step='0.01' name='longitud_espiga' required class='elementos' value='".$regis["longitud_espiga"]."'><br/></label><br/>";
							echo "<!-- Longitud de Espiguilla -->";
							echo "<label class='elementos'>Longitud de Espiguilla<input type='number' step='0.01' name='longitud_espiguilla' required class='elementos' value='".$regis["longitud_espiguilla"]."'><br/></label><br/>";
							echo "<!-- Ancho de Espiguilla -->";
							echo "<label class='elementos'>Ancho de Espiguilla<input type='number' step='0.01' name='ancho_espiguilla' required class='elementos' value='".$regis["ancho_espiguilla"]."'><br/></label><br/>";
							echo "<!-- Nudos mazorca -->";
							echo "<label class='elementos'>Nudos mazorca<input type='number' step='0.01' name='nudos_mazorca' required class='elementos' value='".$regis["nudos_mazorca"]."'><br/></label><br/>";
							echo "<!-- Mazorcas nudo -->";
							echo "<label class='elementos'>Mazorcas nudo<input type='number' step='0.01' name='mazorcas_nudo' required class='elementos' value='".$regis["mazorcas_nudo"]."'><br/></label><br/>";
						}
					}
				?>
                    <!-- SUBMIT -->
					<input name="reg_maiz" form = "maiz" type="submit" value="Editar" class="elementos botonIniciar"><br/>
				</div>
			</form>
		</div>
	</div>
</body>
</html>