<?php
    // Escrito por Arantxa
    /*
    [ Copiado y pegado del PDF en UBICUA ]
    Desplegará la tabla con toda la información de variedadesde flora y fauna, descrita en la Figura 1, pero con la posibilidad de ordenar latabla de acuerdo con el encabezado de cada columna: por nombre deraza/variedad   (alfabéticamente),  por  región  (alfabéticamente),  y   por  cadacaracterística (en orden ascendente). Cada renglón podrá ser editado a peticióndel investigador, pulsando un botón/liga que lo lleve a un formulario dondemanipular los datos del renglón en cuestión.
    
    */
    // Conexión
    require 'conexion.php';
    
    // Variables para determinar con respecto a que columna se ordenan los datos mostrados.
    if(isset($_GET['order'])){
        $order = $_GET['order'];
    }else{
        $order = 'id_comentario'; /* Default, los datos aparecen ordenados por ID*/
    }

    // Variable para determinar si el orden será ascendente o descendente
    if(isset($_GET['sort'])){
        $sort = $_GET['sort'];
    }else{
        $sort = 'ASC';
    }

    // Intercambiar valores de $SORT con cada click
    $sort == 'DESC' ? $sort ='ASC' : $sort = 'DESC';

    // Consulta SQL
    $query = mysqli_query($conexion,"SELECT * FROM comentarios ORDER BY ".$order." ".$sort.";");

?>
<!DOCTYPE html>
<html>
<head>
	<title>Maices</title>
	<link rel="stylesheet" type="text/css" href="scripts/css/estiloVerDatos.css">
</head>
<body>
	<div class="barraNavegacion">
		<ul class="menu">
			<li class="li"><a href="login.php">Salir</a></li>
			<li class="li"><a href="ingresarInv.php">Agregar Investigador</a></li>
            <li class="li"><a href="verDatos.php">Datos</a></li>
            <li class="li"><a href="#" class="activo">Comentarios</a></li>
            <li class="li"><a href="indexAdmin.php">Galer&iacute;a</a></li>
			<li class="li identidad"><img src="img/logo.png" class="logo"></li>
		</ul>
    </div>
<!--INICIO CONTENIDO-->
	<div class="contenido">

        <div class="datos">
            <table id="tablaDatos" style="text-align: left;">
                <tr>
                    <col class="columnasDatos">
                    <col class="columnasDatos">
                    <col class="columnasDatos" style="width: 25%;">
                    <col class="columnasDatos">
                </tr>
                <tr class="encabezadoDatos">
                    <th><a class="linkHead" href="?order=id_comentario&&sort=<?php echo $sort;?>">ID</a></th>
                    <th><a class="linkHead" href="?order=nombre&&sort=<?php echo $sort;?>">Nombre</a></th>
                    <th><a class="linkHead" href="?order='correo-e'&&sort=<?php echo $sort;?>">Correo</a></th>
                    <th><a class="linkHead" href="?order=comentario&&sort=<?php echo $sort;?>">Comentario</a></th>
                </tr>
<!-- INSERTAR DATOS DE LA BD EN TABLA -->
<?php
    while($row = mysqli_fetch_array($query)){
        echo "<tr class='filasDatos'>";
        echo "<td>" . $row['id_comentario'] . "</td>";
        echo "<td>" . $row['nombre'] . "</td>";
        echo "<td>" . $row['correo-e'] . "</td>";
        echo "<td>" . $row['comentario'] . "</td>";
        echo "</tr>";
    }
?>

            </table>
        </div>


    </div>
<!--FIN CONTENIDO-->
    <footer>
        <div id="footer">
            <div id="integrantes">
                <ul class="integrantes">
                    <li>Cabrera Juárez Luz Del Carmen 2123064480</li>
                    <li>Domínguez Lara Andrea 2173070894</li>
                    <li>Garayzar Cristerna Arantxa 2163071727</li>
                    <li>Nieto Rocha Albertos 2163071736</li>
                <ul>
            </div>
            <div id="contacto">
                <p class="contacto"><strong>Información</strong></p>
                <p>UEA: Programación de Web Dinámico <br> 
                Trimestre: 19 Otoño <br>
                Contacto: sotnotatam@maiceros.com</p>
            </div>
        </div>
    </footer>
    
</body>
</html>
