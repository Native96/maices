<!DOCTYPE html>
<html>
<head>
	<title>Ingresar Investigador</title>
	<!-- Estilo general login -->
	<link rel="stylesheet" type="text/css" href="scripts/css/estilosAltaInvestigador.css">
	<!-- Estilo barra de navegacion -->
	<link rel="stylesheet" type="text/css" href="scripts/css/estilosBarraDeNavegacion.css">
	<!-- Estilo footer -->
	<link rel="stylesheet" type="text/css" href="scripts/css/estilosFooter.css">
</head>
<body>
    <!-- Por: Alberto -->
    <div class="barraNavegacion">
        <ul class="menu">
            <li class="li"><a href="login.php">Salir</a></li>
            <li class="li"><a href="#" class="activo">Agregar investigador</a></li>
            <li class="li"><a href="verDatos.php">Datos</a>
            <li class="li"><a href="verComentarios.php">Comentarios</a>
            <li class="li"><a href="indexAdmin.php">Galer&iacute;a</a></li>
            <li class="li identidad"><img src="img/logo.png" class="logo"></li>
        </ul>
    </div>	

	<!-- Por: Luz -->
	<div class="contenedor">	
		<div class="inicio">
			<form action="IngresandoInv.php" method="POST">
				<div class="contenedorTitulo">
					<!--<img src="img/logoLogin.png" alt="Imagen de inicio de usuario" class="imagen"><br/>-->
					<h2>Agregar Investigador</h2>
				</div>
				<div class="contenedorElementos">
					<label class="elementos">Nombre</label><br/>
					<input type="text" name="nombre" required class="elementos"><br/>
					<label class="elementos">Apellido Paterno</label><br/>
					<input type="text" name="apaterno" required class="elementos"><br/>
					<label class="elementos">Apellido Materno</label><br/>
					<input type="text" name="amaterno" required class="elementos"><br/>
					<label class="elementos">Contrase&ntilde;a</label><br/>
					<input type="password" name="contra" required class="elementos"><br/>
					<input type="submit" value="Agregar" class="elementos botonIniciar"><br/>
				</div>
			</form>
		</div>
	</div>

	<!-- Por: Arantxa -->
    <footer>
        <div id="footer">
            <div id="integrantes">
                <ul class="integrantes">
                    <li>Cabrera Juárez Luz Del Carmen 2123064480</li>
                    <li>Domínguez Lara Andrea 2173070894</li>
                    <li>Garayzar Cristerna Arantxa 2163071727</li>
                    <li>Nieto Rocha Albertos 2163071736</li>
                <ul>
            </div>
            <div id="contacto">
                <p class="contacto"><strong>Información</strong></p>
                <p>UEA: Programación de Web Dinámico <br> 
                Trimestre: 19 Otoño <br>
                Contacto: sotnotatam@maiceros.com</p>
            </div>
        </div>
    </footer>
</body>
</html>