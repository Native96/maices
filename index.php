<!-- Por: Alberto, Luz, y Andrea -->
<?php
    require 'conexion.php';
    function mostrarDatos($valor){
        echo $valor;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Maices</title>

    <!-- Estilo general del index -->
    <link rel="stylesheet" type="text/css" href="scripts/css/estiloIndex.css">
    <!-- Estilo barra de navegación -->
    <link rel="stylesheet" href="scripts/css/estilosBarraDeNavegacion.css">
    <!-- Estilo pie de página -->
    <link rel="stylesheet" href="scripts/css/estilosFooter.css">
    <!-- Script que obtiene el id del maiz y lo envia por medio de un php -->
    <!-- Luz, Andrea -->
    <script type="text/javascript">
        function obtener_objeto(){
            if (window.XMLHttpRequest){
                    return new XMLHttpRequest();
                }else if (windows.ActiveXObject){
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }else{
                    alert("No soporta");
                }
        }
        var solicitud=obtener_objeto(); 
        var idj;
        function peticion(idj){
            if (solicitud.readyState==0 || solicitud.readyState==4){
                //envia el id hacia prueba.php
                    solicitud.open("GET","datos.php?id="+idj,true);
                    solicitud.onreadystatechange=mostrar;
                    solicitud.send(null);
                }
            }
        function mostrar(){
            if (solicitud.readyState==4){
                document.getElementById('galeria').innerHTML=solicitud.responseText;
            }
        }
    </script>
</head>
    <body>

        <!-- Por: Alberto -->
        <div class="barraNavegacion">
            <ul class="menu">
                <li class="li"><a href="comentarios.php">Contacto</a></li>
                <li class="li"><a href="mapa.php">Mapa</a></li>
                <li class="li"><a href="#" class="activo">Galer&iacute;a</a></li>
                <li class="li identidad"><img src="img/logo.png" class="logo"></li>
            </ul>
        </div>

        <!-- Por: Luz, y Andrea -->
        <div class="contenido">
            <?php 
                # Consulta en la base de datos, tabla maiz.
                $consulta = "SELECT * FROM maiz order by nombre";
                $resultado = $conexion->query($consulta);
                # Conteo de filas de la tabla.
                $filas = $resultado->num_rows;
                # Si hay fila, imprimir cada renglo.
                if($filas > 0){
                    echo "<div class='contenedorNombres'>";
                    echo "<table class='tablaNombres'>";
                    while($registro = $resultado->fetch_assoc()){
                        # Por: Alberto
                        echo "<tr>" .$nombres = "<td class='nombres' value='" .$registro['id_maiz'] ."'onmouseover='remarcar(this);' onmouseout='original(this);'><a class='enlaces' href='javascript:peticion(".$registro["id_maiz"].");'>" .$registro["nombre"] ."</a></td></tr>";
                    }
                    echo "</table></div>";
                }else{
                    echo "<p color='red'>Sin resultado</p>";
                }
            ?>
            <div id="galeria">
            </div>
        </div>
        
        <!-- Por: Arantxa -->
        <footer>
            <div id="footer">
                <div id="integrantes">
                    <ul class="integrantes">
                        <li>Cabrera Juárez Luz Del Carmen 2123064480</li>
                        <li>Domínguez Lara Andrea 2173070894</li>
                        <li>Garayzar Cristerna Arantxa 2163071727</li>
                        <li>Nieto Rocha Albertos 2163071736</li>
                    <ul>
                </div>
                <div id="contacto">
                    <p class="contacto"><strong>Información</strong></p>
                    <p>UEA: Programación de Web Dinámico <br> 
                    Trimestre: 19 Otoño <br>
                    Contacto: sotnotatam@maiceros.com</p>
                </div>
            </div>
        </footer>
        <script type="text/javascript" src="scripts/javascript/index.js"></script>
    </body>
</html>