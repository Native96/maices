--
-- Base de datos: `Maices`
--
CREATE DATABASE IF NOT EXISTS `Maices` DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE `Maices`;

DROP USER IF EXISTS 'sotnotatam'@'localhost';
CREATE USER IF NOT EXISTS 'sotnotatam'@'localhost' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON *.* TO 'sotnotatam'@'localhost' REQUIRE NONE
WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0
MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `Maices`.* TO 'sotnotatam'@'localhost';

-- --------------------------------------------------------
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id_comentario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `correo-e` varchar(35) COLLATE latin1_spanish_ci NOT NULL,
  `comentario` text COLLATE latin1_spanish_ci NOT NULL,
	PRIMARY KEY (id_comentario) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='lmacena los comentarios vertidos en la página por  usuarios';

-- --------------------------------------------------------


--
-- Estructura de tabla para la tabla `investigador`
--

CREATE TABLE `investigador` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `apaterno` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `amaterno` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `contra` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
	PRIMARY KEY (id_usuario) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Datos de los administradores del sitio: investigadores';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maiz`
--

CREATE TABLE `maiz` (
  `id_maiz` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(35) COLLATE latin1_spanish_ci NOT NULL,
  `imagen` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `longitud_hoja` float NOT NULL, 
  `anchura_hoja` float NOT NULL, 
  `longitud_espiga` float NOT NULL, 
  `longitud_espiguilla` float NOT NULL, 
  `ancho_espiguilla` float NOT NULL, 
  `nudos_mazorca` float NOT NULL, 
  `mazorcas_nudo` float NOT NULL,
	PRIMARY KEY (id_maiz) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Es la tabla de datos principal que alberga los datos de los ';

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE `region` (
  `id_region` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
	PRIMARY KEY (id_region) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maiz-region`
--

CREATE TABLE `maiz-region` (
  `id_region` int(11) NOT NULL,
  `id_maiz` int(11) NOT NULL,
  CONSTRAINT `maiz-region_ibfk_1` FOREIGN KEY (`id_maiz`) REFERENCES `maiz` (`id_maiz`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `maiz-region_ibfk_2` FOREIGN KEY (`id_region`) REFERENCES `region` (`id_region`)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT PK_maiz_region PRIMARY KEY (`id_region`,`id_maiz`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


-- --------------------------------------------------------

-- 
-- INSERTS 
--
ALTER TABLE `region` AUTO_INCREMENT = 1;

INSERT INTO `region` 
(estado)
VALUES
("Aguascalientes"),
("Baja California"),
("Baja California Sur"),
("Campeche"),
("Chihuahua"),
("Chiapas"),
("Ciudad de México"),
("Coahuila"),
("Colima"),
("Durango"),
("Guanajuato"),
("Guerrero"),
("Hidalgo"),
("Jalisco"),
("Estado de México"),
("Michoacán"),
("Morelos"),
("Nayarit"),
("Nuevo León"),
("Oaxaca"),
("Puebla"),
("Querétaro"),
("Quintana Roo"),
("San Luis Potosí"),
("Sinaloa"),
("Sonora"),
("Tabasco"),
("Tamaulipas"),
("Tlaxcala"),
("Veracruz"),
("Yucatán"),
("Zacatecas");


ALTER TABLE `maiz` AUTO_INCREMENT = 1;

INSERT INTO `maiz` 
(nombre,longitud_hoja, anchura_hoja, longitud_espiga, longitud_espiguilla, ancho_espiguilla, nudos_mazorca, mazorcas_nudo, imagen)
VALUES
("Palomero", 6,5,71,52,16,8,3,"palomero.jpg"),
("Arrocillo", 6,4,40,97,8,3,3,"arrocillo.jpg"),
("Chapalote", 4,10,92,47,26,6,5,"chapalote.jpg"),
("Nal-Tel", 7,8,82,97,15,10,5,"naltel.jpg"),
("Cacahuacintle", 4,3,96,39,20,8,4,"cacahuacintle.jpg"),
("Harinoso de ocho", 10,5,37,81,28,9,5,"ocho.jpg"),
("Olotón", 2,8,14,42,6,7,3,"oloton.jpg"),
("Maíz Dulce", 8,1,46,86,17,4,4,"dulce.jpg"),
("Cónico", 5,7,40,80,27,10,2,"conico.jpg"),
("Reventador", 5,10,99,46,15,9,4,"reventador.jpg"),
("Tabloncillo", 2,6,97,56,10,7,3,"tabloncillo.jpg"),
("Tehua", 2,9,48,52,24,4,3,"tehua.jpg"),
("Tepecintle", 8,1,47,79,13,2,2,"tepecintle.jpg"),
("Comiteco", 7,8,36,17,26,7,2,"comiteco.jpg"),
("Jala", 5,1,73,47,23,7,5,"jala.jpg"),
("Zapalote chico", 2,3,49,51,10,8,4,"zapalotec.jpg"),
("Zapalote Grande", 9,4,51,90,6,7,4,"zapaloteg.jpg"),
("Pepitilla", 2,5,86,17,21,2,4,"pepitilla.jpg"),
("Olotillo", 2,8,39,86,15,4,2,"olotillo.jpg"),
("Tuxpeño", 7,3,14,61,20,4,3,"tuxpeno.jpg"),
("Vandeño", 6,5,65,18,34,1,3,"vandeno.jpg"),
("Chalqueño", 8,4,38,22,14,9,4,"chalqueno.jpg"),
("Celaya", 8,6,55,77,29,7,3,"celaya.jpg"),
("Cónico Norteño", 7,10,79,18,13,5,1,"conicoN.jpg"),
("Bolita", 2,4,92,73,10,6,1,"bolita.jpg"),
("Conejo", 1,4,100,36,14,5,4,"conejo.jpg"),
("Mushito",  9,10,59,69,27,3,3,"mushito.jpg"),
("Complejo Serrano de Jalisco", 9,5,98,82,25,2,2,"compsej.jpg"),
("Zamorano Amarillo", 1,1,54,29,26,9,2,"zamorano.jpg"),
("Blando de Sonora", 8,2,50,13,8,5,4,"blando.jpg"),
("Onaveño", 4,10,58,21,22,2,4,"onaveño.jpg"),
("Dulcillo del Noroeste", 7,7,100,67,31,6,3,"dulcillos.jpg"),
("Teocintle", 3,8,53,29,18,8,3,"teocintle.jpg");


INSERT INTO `maiz-region` 
(id_region, id_maiz)
VALUES
(1, 9),
(1, 22),
(1, 24),
(3, 11),
(4, 19),
(4, 20),
(4, 24),
(5, 9),
(5, 20),
(5, 33),
(6, 5),
(6, 7),
(6, 12),
(6, 13),
(6, 14),
(6, 16),
(6, 17),
(6, 19),
(6, 21),
(6, 33),
(7, 9),
(7, 18),
(7, 32),
(7, 33),
(8, 20),
(9, 10),
(9, 28),
(9, 32),
(10, 8),
(10, 22),
(10, 33),
(11, 8),
(11, 9),
(11, 23),
(11, 24),
(11, 33),
(12, 4),
(12, 10),
(12, 19),
(12, 21),
(12, 26),
(12, 32),
(12, 33),
(13, 20),
(13, 22),
(14, 1),
(14, 7),
(14, 8),
(14, 9),
(14, 10),
(14, 11),
(14, 15),
(14, 18),
(14, 23),
(14, 24),
(14, 28),
(14, 32),
(14, 33),
(15, 1),
(15, 5),
(15, 9),
(15, 18),
(15, 22),
(15, 33),
(16, 8),
(16, 9),
(16, 10),
(16, 11),
(16, 18),
(16, 19),
(16, 21),
(16, 22),
(16, 23),
(16, 26),
(16, 29),
(16, 32),
(16, 33),
(17, 18),
(17, 33),
(18, 6),
(18, 8),
(18, 10),
(18, 11),
(18, 15),
(18, 21),
(18, 32),
(19, 20),
(20, 4),
(20, 13),
(20, 16),
(20, 17),
(20, 18),
(20, 19),
(20, 20),
(20, 21),
(20, 22),
(20, 25),
(20, 27),
(21, 1),
(21, 2),
(21, 5),
(21, 9),
(21, 18),
(21, 20),
(21, 22),
(21, 25),
(22, 8),
(23, 20),
(24, 4),
(24, 9),
(24, 20),
(25, 3),
(25, 10),
(25, 11),
(25, 32),
(26, 3),
(26, 6),
(26, 8),
(26, 10),
(26, 11),
(26, 20),
(26, 30),
(26, 31),
(26, 32),
(27, 20),
(28, 20),
(28, 21),
(29, 9),
(29, 22),
(30, 1),
(30, 9),
(30, 19),
(30, 20),
(30, 21),
(31, 4),
(31, 19),
(31, 20),
(32, 9),
(32, 22);

INSERT INTO `investigador` 
(nombre,apaterno, amaterno, contra)
VALUES
("Andrea", "Dominguez", "Lara", "adli"),
("Arantxa", "Garayzar", "Cristerna", "agci"),
("Alberto", "Nieto", "Rocha", "anri"),
("Andrea", "Dominguez", "Lara", "adli"),
("Luz", "Cabrera", "Juarez", "lcji");
