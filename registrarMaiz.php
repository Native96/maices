<?php 
    // Registro de nuevo maíz.
    // Plantilla realizada por Alberto Nieto Rocha
    // Formulario y php escrito por Arantxa Garayzar
    // Descripción: Muestra formulario al usuario Admin para agregar un nuevo registro de una raza de maíz
    //              ya que el usuario termine de llenar los datos, la sección php se encarga de procesar esos
	//              datos e ingresarlos a la BD maices. 
	
	// Sesión
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Convocatoria - inicio administrado</title>
	<link rel="stylesheet" type="text/css" href="scripts/css/estiloRegistrarMaiz.css">
</head>
<body>
	<div class="contenedor">	
		<div class="inicio">
			<form action="./validaMaiz.php" method="POST" id="maiz" >
				<div class="contenedorImagen">
                    <!-- Logotipo -->
					<img src="img/logoLogin.png" alt="Imagen de inicio de usuario" class="imagen"><br/>
				</div>
				<div class="contenedorElementos">
<!-- Nombre del maiz -->
					<label class="elementos">Nombre del maíz<input type="text" name="nombre" required class="elementos"><br/></label><br/>
<!-- Seleccionar img -->
                    <label class="elementos">Seleccionar imagen<input type="file" name="imagen" id ="imagen" required class="elementos"><br/></label><br/>
<!-- Longitud de Hoja -->
                    <label class="elementos">Longitud de hoja<input type="number" step="0.01" name="longitud_hoja" required class="elementos"><br/></label><br/>
<!-- Anchura Hoja -->
                    <label class="elementos">Anchura Hoja<input type="number" step="0.01" name="anchura_hoja" required class="elementos"><br/></label><br/>
<!-- Longitud de Espiga -->
                    <label class="elementos">Longitud de Espiga<input type="number" step="0.01" name="longitud_espiga" required class="elementos"><br/></label><br/>
<!-- Longitud de Espiguilla -->
					<label class="elementos">Longitud de Espiguilla<input type="number" step="0.01" name="longitud_espiguilla" required class="elementos"><br/></label><br/>
<!-- Ancho de Espiguilla -->
					<label class="elementos">Ancho de Espiguilla<input type="number" step="0.01" name="ancho_espiguilla" required class="elementos"><br/></label><br/>
<!-- Nudos mazorca -->
					<label class="elementos">Nudos mazorca<input type="number" step="0.01" name="nudos_mazorca" required class="elementos"><br/></label><br/>
<!-- Mazorcas nudo -->
					<label class="elementos">Mazorcas nudo<input type="number" step="0.01" name="mazorcas_nudo" required class="elementos"><br/></label><br/>

                    <!-- SUBMIT -->
					<input name="reg_maiz" form = "maiz" type="submit" value="Iniciar Sesi&oacute;n" class="elementos botonIniciar"><br/>
				</div>
			</form>
		</div>
	</div>
</body>
</html>